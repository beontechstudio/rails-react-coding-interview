import { FC } from 'react';
import { styled as muStyled } from '@mui/material';
import Navbar from './Navbar';

const LayoutWrapper = muStyled('main')(({ theme }) => ({
    display: 'flex',
    flex: '1',
    overflow: 'hidden',
}));

const LayoutContainer = muStyled('div')({
    display: 'flex',
    flex: '1',
    overflow: 'hidden',
});

const LayoutContent = muStyled('div')({
    flex: '1',
    overflow: 'auto',
    position: 'relative',
    WebkitOverflowScrolling: 'touch',
    display: 'flex',
    flexDirection: 'column',

    '> *': {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
});

const Layout: FC = ({ children }) => {
    return (
        <>
            <Navbar />

            <LayoutWrapper>
                <LayoutContainer>
                    <LayoutContent>{children}</LayoutContent>
                </LayoutContainer>
            </LayoutWrapper>
        </>
    );
};

export default Layout;
