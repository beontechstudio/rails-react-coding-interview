import { FC } from 'react';
import { AppBar, Button, Toolbar } from '@mui/material';
import { RouteComponentProps } from '@reach/router';

const Navbar: FC<RouteComponentProps> = () => {
    return (
        <AppBar position="static">
            <Toolbar style={{ display: 'flex' }}>
                <img
                    src="https://beon.studio/assets/img/logo_beon_white.png"
                    style={{ marginRight: 'auto', width: '150px' }}
                />

                <Button color="inherit" href="/">
                    Flights
                </Button>
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;
