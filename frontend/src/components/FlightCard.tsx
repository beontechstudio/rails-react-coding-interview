import { FC } from 'react';
import { Box, Card, Typography } from '@mui/material';
import { FlightModel, FlightStatuses } from '../models/flight.model';

const mapFlightStatusToColor = (status: FlightStatuses) => {
    const mappings = {
        [FlightStatuses.Arrived]: '#1ac400',
        [FlightStatuses.Delayed]: '##c45800',
        [FlightStatuses['On Time']]: '#1ac400',
        [FlightStatuses.Landing]: '#1ac400',
        [FlightStatuses.Cancelled]: '#ff2600',
    };

    return mappings[status] || '#000000';
};

interface Props {
    flight: FlightModel;
}

const FlightCard: FC<Props> = (props) => {
    const { flight } = props;

    return (
        <Card
            style={{
                backgroundColor: '#f5f5f5',
                margin: '15px',
                padding: '35px',
                justifyContent: 'center',
            }}
        >
            <Box style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h5">{flight.code} </Typography>
                <Typography
                    style={{ color: mapFlightStatusToColor(flight.status) }}
                >
                    Status: {flight.status}
                </Typography>
            </Box>

            <Box>
                <Typography>Origin: {flight.origin}</Typography>
            </Box>
            <Box>
                <Typography>Destination: {flight.destination}</Typography>
            </Box>
        </Card>
    );
};

export default FlightCard;
