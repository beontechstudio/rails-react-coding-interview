export enum FlightStatuses {
    Arrived = 'Arrived',
    Landing = 'Landing',
    'On Time' = 'On Time',
    Delayed = 'Delayed',
    Cancelled = 'Cancelled',
}

export class FlightModel {
    code: string;
    origin: string;
    destination: string;
    status: FlightStatuses;
    passengers?: string[];

    constructor(data: FlightModel | Record<string, any>) {
        this.code = data.code;
        this.origin = data.origin;
        this.destination = data.destination;
        this.status = data.status;
        this.passengers = data.passengers || [];
    }
}
