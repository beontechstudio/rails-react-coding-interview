import { FC } from 'react';
import { Box, Typography } from '@mui/material';
import { RouteComponentProps } from '@reach/router';
import { useFlights } from '../hooks/flights.hook';
import FlightCard from '../components/FlightCard';

const FlightsPage: FC<RouteComponentProps> = () => {
    const { flights } = useFlights();

    return (
        <Box>
            <Box>
                <Typography marginLeft="15px" marginRight="15px" variant="h4">
                    Scheduled Flights
                </Typography>
            </Box>

            <Box>
                {flights.length
                    ? flights.map((flight) => (
                          <FlightCard key={flight.code} flight={flight} />
                      ))
                    : null}
            </Box>
        </Box>
    );
};

export default FlightsPage;
