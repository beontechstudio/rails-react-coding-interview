import { FlightModel, FlightStatuses } from '../models/flight.model';

// const FAKER_API = 'https://fakerapi.it/api/v1/custom?_quantity=20&code=number&origin=country&destination=country';
const BASE_URL = 'http://localhost:3001';

export class BackendClient {
    private static instance: BackendClient;

    static getInstance(): BackendClient {
        if (!BackendClient.instance) {
            BackendClient.instance = new BackendClient();
        }
        return BackendClient.instance;
    }

    async getFlights(): Promise<FlightModel[]> {
        const response = await fetch(`${BASE_URL}/flights`);
        const data = await response.json();

        return data.data.map(
            (flight: any) =>
                new FlightModel({
                    ...flight,
                    status: FlightStatuses.Arrived,
                    passengers: [],
                }),
        );
    }
}
