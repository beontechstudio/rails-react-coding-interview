import { useEffect, useState } from 'react';
import { BackendClient } from '../clients/backend.client';
import { FlightModel, FlightStatuses } from '../models/flight.model';

const backendClient = BackendClient.getInstance();

export const useFlights = () => {
    const [flights, setFlights] = useState<FlightModel[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<any | null>(null);

    const getFlights = async () => {
        setIsLoading(true);
        try {
            const data = await backendClient.getFlights();
            setFlights(data);
        } catch (error) {
            setError(error);
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        getFlights();
    }, []);

    return { flights, isLoading, error };
};
