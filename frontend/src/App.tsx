import { Router } from '@reach/router';
import Layout from './components/Layout';
// pages
import FlightsPage from './pages/Flights.page';

function App() {
    return (
        <Layout>
            <Router>
                <FlightsPage path="/flights" />
            </Router>
        </Layout>
    );
}

export default App;
