FactoryBot.define do
  factory :flight do
    code { "AV-#{Faker::Number.unique.rand(1000)}" }
    destination { Airport.find_by(code: 'LAX') }
    origin { Airport.find_by(code: 'DFW') }
  end
end
