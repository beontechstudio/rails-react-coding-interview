require 'rails_helper'

RSpec.describe "Flights", type: :request do
  let(:result) { JSON.parse(response.body) }

  describe "#index" do
    let!(:flights) { create_list(:flight, 2) }

    context 'when fetching without filters' do

      it 'returns all the active flights' do
        get flights_path

        expect(result['data'].size).to eq(flights.size)
        expect(result['data'].map { |element| element['id'] } ).to eq(flights.map { |f| f.id })
      end
    end
  end
end
