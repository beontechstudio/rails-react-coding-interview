# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

airport_list = [
  {
    code: 'ATL',
    name: 'Hartsfield–Jackson Atlanta International Airport',
    city: 'Atlanta'
  },
  {
    code: 'LAX',
    name: 'Los Angeles International Airport',
    city: 'Los Angeles'
  },
  {
    code: 'DFW',
    name: 'Dallas/Fort Worth International Airport',
    city: 'Dallas-Fort Worth'
  }
]

airport_list.each do |airport_data|
  airport =  Airport.find_or_initialize_by(code: airport_data[:code])
  airport.update!(airport_data.except(:code))
end