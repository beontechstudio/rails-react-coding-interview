class CreateFlights < ActiveRecord::Migration[7.0]
  def change
    create_table :flights do |t|
      t.string :code, null: false
      t.references :origin, index: true, null: false, foreign_key: { to_table: :airports}
      t.references :destination, index: true, null: false, foreign_key: { to_table: :airports }
      t.integer :status, null: false, default: 0
      t.timestamps
    end
  end
end
