class FlightsController < ApplicationController

  def index
    @flights = Flight.all
    render json: { data: @flights }
  end
end
