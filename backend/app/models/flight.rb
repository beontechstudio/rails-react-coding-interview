class Flight < ApplicationRecord
  belongs_to :origin, class_name: 'Airport', foreign_key: 'origin_id'
  belongs_to :destination, class_name: 'Airport', foreign_key: 'destination_id'

  enum status: { scheduled: 0, on_time: 1, landing: 2, arrived: 3 }
end
